﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DipInj
{
    public class DrawStar : IDraw
    {
        public void Draw()
        {
            
            for (int i = 1; i <= 8; i++)
            {
                for (int j = 1; j <= 8; j++)

                {
                    if ((i == 1 || i == 8) || (j == 1 || j == 8))
                        Console.Write("* ");
                    else
                        Console.Write("  "); 
                }
                Console.WriteLine();
            }
        }
        
    }
}
