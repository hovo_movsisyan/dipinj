﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipInj
{
    class ToDraw
    {
        IDraw _draw = null;

        public ToDraw(IDraw draw)
        {
            _draw = draw;
        }

        public void Nkarel()
        {
            _draw.Draw();
        }
    }
}
