﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ninject;
using Ninject.Modules;

namespace DipInj
{
    class Program
    {
        
        
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            IDraw draw = kernel.Get<IDraw>();
            ToDraw toDraw = new ToDraw(draw);
            toDraw.Nkarel();
            Console.ReadKey();
        }
    }
    public class Bindings : NinjectModule
    {
        public  override void Load()
        {
            Bind<IDraw>().To<Draw8>();
                
        }

       
    }
}
