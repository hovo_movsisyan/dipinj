﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipInj
{
    public class Draw8 : IDraw
    {
        public void Draw()
        {
            for (int i = 1; i <= 8; i++)
            {
                for (int j = 1; j <= 8; j++)

                {
                    if ((i == 1 || i == 8) || (j == 1 || j == 8))
                        Console.Write("8 ");
                    else
                        Console.Write("  ");
                }
                Console.WriteLine();
            }
        }

       
    }
}
